#!/usr/bin/env bash

# ---
# Axel Latvala, Espoo, Feb 2017
# ---

MINPAGE=1
MAXPAGE=129
HOST="lannistajakuha.com"
PROTO="http"
LINENUM=73
for i in $(eval echo "{$MINPAGE..$MAXPAGE}")
do
    URL="$PROTO://$HOST/$i"
    PAGEDATA=$(curl -sL "$URL")
    if [[ $? -ne 0 ]]; then
        # retry
        PAGEDATA=$(curl -sL "$URL")
        if [[ $? -ne 0 ]]; then
            >&2 echo "Error loading url $URL"
        else
            >&1 echo "$PAGEDATA" | sed -n "$LINENUM"p
        fi
    else
        >&1 echo "$PAGEDATA" | sed -n "$LINENUM"p
    fi
done
